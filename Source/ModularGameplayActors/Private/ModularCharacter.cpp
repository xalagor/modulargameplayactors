// Copyright Epic Games, Inc. All Rights Reserved.

#include "ModularCharacter.h"
#include "Components/GameFrameworkComponentManager.h"

void AModularCharacter::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (const auto* GI = GetGameInstance())
	{
		if (auto* System = GI->GetSubsystem<UGameFrameworkComponentManager>())
		{
			System->AddReceiver(this);
		}
	}
}

void AModularCharacter::BeginPlay()
{
	// UE5 specific
	// UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(this, UGameFrameworkComponentManager::NAME_GameActorReady);

	Super::BeginPlay();
}

void AModularCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto* System = GetGameInstance()->GetSubsystem<UGameFrameworkComponentManager>())
	{
		System->RemoveReceiver(this);
	}

	Super::EndPlay(EndPlayReason);
}
