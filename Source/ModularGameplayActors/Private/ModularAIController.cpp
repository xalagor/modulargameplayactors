// Copyright Epic Games, Inc. All Rights Reserved.

#include "ModularAIController.h"
#include "Components/GameFrameworkComponentManager.h"

void AModularAIController::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (const auto* GI = GetGameInstance())
	{
		if (auto* System = GI->GetSubsystem<UGameFrameworkComponentManager>())
		{
			System->AddReceiver(this);
		}
	}
}

void AModularAIController::BeginPlay()
{
	// UE5 specific
	// UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(this, UGameFrameworkComponentManager::NAME_GameActorReady);

	Super::BeginPlay();
}

void AModularAIController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto* System = GetGameInstance()->GetSubsystem<UGameFrameworkComponentManager>())
	{
		System->RemoveReceiver(this);
	}

	Super::EndPlay(EndPlayReason);
}
