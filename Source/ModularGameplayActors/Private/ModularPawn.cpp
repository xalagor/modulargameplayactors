// Copyright Epic Games, Inc. All Rights Reserved.

#include "ModularPawn.h"
#include "Components/GameFrameworkComponentManager.h"

void AModularPawn::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (const auto* GI = GetGameInstance())
	{
		if (auto* System = GI->GetSubsystem<UGameFrameworkComponentManager>())
		{
			System->AddReceiver(this);
		}
	}
}

void AModularPawn::BeginPlay()
{
	// UE5 specific
	// UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(this, UGameFrameworkComponentManager::NAME_GameActorReady);

	Super::BeginPlay();
}

void AModularPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto* System = GetGameInstance()->GetSubsystem<UGameFrameworkComponentManager>())
	{
		System->RemoveReceiver(this);
	}

	Super::EndPlay(EndPlayReason);
}
