// Copyright Epic Games, Inc. All Rights Reserved.

#include "ModularPlayerController.h"
#include "Components/GameFrameworkComponentManager.h"
#include "Components/ControllerComponent.h"

void AModularPlayerController::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	if (const auto* GI = GetGameInstance())
	{
		if (auto* System = GI->GetSubsystem<UGameFrameworkComponentManager>())
		{
			System->AddReceiver(this);
		}
	}
}

void AModularPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto* System = GetGameInstance()->GetSubsystem<UGameFrameworkComponentManager>())
	{
		System->RemoveReceiver(this);
	}

	Super::EndPlay(EndPlayReason);
}

void AModularPlayerController::ReceivedPlayer()
{
	// Player controllers always get assigned a player and can't do much until then
	// UE5 specific
	// UGameFrameworkComponentManager::SendGameFrameworkComponentExtensionEvent(this, UGameFrameworkComponentManager::NAME_GameActorReady);

	Super::ReceivedPlayer();

	TArray<UControllerComponent*> ModularComponents;
	GetComponents(ModularComponents);
	for (UControllerComponent* Component : ModularComponents)
	{
		Component->ReceivedPlayer();
	}
}

void AModularPlayerController::PlayerTick(const float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	TArray<UControllerComponent*> ModularComponents;
	GetComponents(ModularComponents);
	for (UControllerComponent* Component : ModularComponents)
	{
		Component->PlayerTick(DeltaTime);
	}
}
